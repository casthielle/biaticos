import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { ReportsPage } from '../pages/reports/reports';
import { Globals } from '../providers/globals';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{icon: string, title: string, component: any, badge: any}>;

  public user = {};

  public expenses: Array<{
    provider: string,
    image: string,
    description: string,
    date: string,
    category: string,
    ticket: string,
    amount: number,
    currency: string,
    ccoo: string,
    note: string,
    reported: boolean,
    reportID: string
  }>;

  public reports: Array<{
    id: string,
    title: string,
    note: string,
    approved: boolean,
    user: string
  }> = [];

  constructor(public platform: Platform, public globals: Globals, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();
    this.statusBar.backgroundColorByHexString('#000000');

    this.expenses = this.globals.get('expenses');
    this.reports = this.globals.get('reports');

    this.user = this.globals.get('session');

    this.test();

    let badge_reports = this.globals.get("reports").length;
    console.log(badge_reports);

    // used for an example of ngFor and navigation
    this.pages = [
      { icon: 'ios-document-outline', title: 'Gastos', component: HomePage, badge: 0 },
      { icon: 'ios-paper-outline', title: 'Solicitud de Aprobaciones', component: ReportsPage, badge: badge_reports },
      { icon: 'ios-images-outline', title: 'Galería', component: ListPage, badge: 0 },
      { icon: 'ios-power-outline', title: 'Modo Sin Conexión', component: ListPage, badge: 0 },
      { icon: 'ios-settings-outline', title: 'Configuracion', component: ListPage, badge: 0 },
      { icon: 'ios-help-circle-outline', title: 'Ayuda', component: ListPage, badge: 0 },
      { icon: 'md-arrow-dropright-circle', title: 'Plan Empresas', component: ListPage, badge: 0 }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  test(){

    let report1 = {
      id: "333",
      title: "Traslado a Sede",
      note: null,
      approved: null,
      user: "Nicolas Lass"
    };
    let report2 = {
      id: "334",
      title: "Viaje de negocios",
      note: null,
      approved: null,
      user: "Jose Montaño"
    };
    let report3 = {
      id: "335",
      title: "Capacitacion Uruguay",
      note: null,
      approved: null,
      user: "John Knight"
    };
    let report4 = {
      id: "336",
      title: "Reintegro Odontologia",
      note: null,
      approved: null,
      user: "Hector Zea"
    };
    let report5 = {
      id: "337",
      title: "Comida con clientes",
      note: null,
      approved: null,
      user: "Uriel Hayon"
    };
    let report6 = {
      id: "338",
      title: "Traslado a Clientes",
      note: null,
      approved: null,
      user: "Gabriel Contreras"
    };
    let report7 = {
      id: "339",
      title: "Operacion",
      note: null,
      approved: null,
      user: "Lucas Gaitan"
    };

    if(this.reports.length === 0){
      this.globals.add('reports', report1);
      this.globals.add('reports', report2);
      this.globals.add('reports', report3);
      this.globals.add('reports', report4);
      this.globals.add('reports', report5);
      this.globals.add('reports', report6);
      this.globals.add('reports', report7);
    }

    let data1 = {
      provider: "Churreria",
      image: "assets/test.jpg",
      description: "2 Sandwichs",
      date: "2017-09-11",
      category: "Transporte",
      ticket: "12418515",
      amount: 75.00,
      currency: "clp",
      ccoo: "Finanzas",
      note: "fsdfasdf",
      reported: false,
      reportID: null
    };
    let data2 = {
      provider: "Reserva Patagonia",
      image: "assets/test.jpg",
      description: "4 Cervezas",
      date: "2017-09-11",
      category: "Transporte",
      ticket: "12418515",
      amount: 380.00,
      currency: "clp",
      ccoo: "Finanzas",
      note: "fsdfasdf",
      reported: false,
      reportID: null
    };
    let data3 = {
      provider: "Taxi",
      image: "assets/test.jpg",
      description: "Traslado",
      date: "2017-09-11",
      category: "Transporte",
      ticket: "12418515",
      amount: 380.00,
      currency: "clp",
      ccoo: "Finanzas",
      note: "fsdfasdf",
      reported: true,
      reportID: "333"
    };
    let data5 = {
      provider: "Dr. Jose Betini",
      image: "assets/test.jpg",
      description: "Articulos Varios",
      date: "2017-09-11",
      category: "Transporte",
      ticket: "12418515",
      amount: 680.00,
      currency: "clp",
      ccoo: "Finanzas",
      note: "fsdfasdf",
      reported: true,
      reportID: "339"
    };
    let data6 = {
      provider: "Aereolineas Argentinas",
      image: "assets/test.jpg",
      description: "Vuelo BA - CSS",
      date: "2017-09-11",
      category: "Transporte",
      ticket: "12418515",
      amount: 9300.00,
      currency: "clp",
      ccoo: "Finanzas",
      note: "fsdfasdf",
      reported: true,
      reportID: "334"
    };
    let data7 = {
      provider: "Taxi",
      image: "assets/test.jpg",
      description: "Traslado a sede",
      date: "2017-09-11",
      category: "Transporte",
      ticket: "12418515",
      amount: 210.00,
      currency: "clp",
      ccoo: "Finanzas",
      note: "fsdfasdf",
      reported: true,
      reportID: "334"
    };
    let data4 = {
      provider: "Doña Pancha",
      image: "assets/test.jpg",
      description: "Almuerzo",
      date: "2017-09-11",
      category: "Transporte",
      ticket: "12418515",
      amount: 400.00,
      currency: "clp",
      ccoo: "Finanzas",
      note: "fsdfasdf",
      reported: true,
      reportID: "333"
    };
    let data8 = {
      provider: "TECHNET",
      image: "assets/test.jpg",
      description: "Capacitacion JS",
      date: "2017-09-11",
      category: "Transporte",
      ticket: "12418515",
      amount: 3450.00,
      currency: "clp",
      ccoo: "Finanzas",
      note: "fsdfasdf",
      reported: true,
      reportID: "335"
    };
    let data9 = {
      provider: "TECHNET",
      image: "assets/test.jpg",
      description: "Capacitacion .Net",
      date: "2017-09-11",
      category: "Transporte",
      ticket: "12418515",
      amount: 4200.00,
      currency: "clp",
      ccoo: "Finanzas",
      note: "fsdfasdf",
      reported: true,
      reportID: "335"
    };
    let data10 = {
      provider: "Dr. Demian",
      image: "assets/test.jpg",
      description: "Blanqueamiento",
      date: "2017-09-11",
      category: "Transporte",
      ticket: "12418515",
      amount: 980.00,
      currency: "clp",
      ccoo: "Finanzas",
      note: "fsdfasdf",
      reported: true,
      reportID: "336"
    };
    let data12 = {
      provider: "Antares",
      image: "assets/test.jpg",
      description: "2 pizzas",
      date: "2017-09-11",
      category: "Transporte",
      ticket: "12418515",
      amount: 180.00,
      currency: "clp",
      ccoo: "Finanzas",
      note: "fsdfasdf",
      reported: true,
      reportID: "337"
    };
    let data14 = {
      provider: "Taxi",
      image: "assets/test.jpg",
      description: "Traslado a Sede",
      date: "2017-09-11",
      category: "Transporte",
      ticket: "12418515",
      amount: 560.00,
      currency: "clp",
      ccoo: "Finanzas",
      note: "fsdfasdf",
      reported: true,
      reportID: "338"
    };
    let data16 = {
      provider: "Farmacia Lujan",
      image: "assets/test.jpg",
      description: "Anestesicos Varios",
      date: "2017-09-11",
      category: "Transporte",
      ticket: "12418515",
      amount: 160.00,
      currency: "clp",
      ccoo: "Finanzas",
      note: "fsdfasdf",
      reported: true,
      reportID: "339"
    };

    if(this.expenses.length === 0){
      this.globals.add('expenses', data1);
      this.globals.add('expenses', data2);
      this.globals.add('expenses', data3);
      this.globals.add('expenses', data4);
      this.globals.add('expenses', data5);
      this.globals.add('expenses', data6);
      this.globals.add('expenses', data7);
      this.globals.add('expenses', data8);
      this.globals.add('expenses', data9);
      this.globals.add('expenses', data10);
      this.globals.add('expenses', data12);
      this.globals.add('expenses', data14);
      this.globals.add('expenses', data16);
    }
  }
}
