import { Injectable } from '@angular/core';

@Injectable()

export class Globals {

  public expenses: Array<{
    provider: string,
    image: string,
    description: string,
    date: string,
    category: string,
    ticket: string,
    amount: number,
    currency: string,
    ccoo: string,
    note: string,
    reported: boolean,
    reportID: string,
  }> = [];

  public reports: Array<{
    id: string,
    title: string,
    note: string,
    approved: boolean,
    user: string,
    date: string
  }> = [];

  public session = {
    user: null,
    image: null,
    email: null
  };

  constructor(){
  }

  add(array, item) {
    this[array].push(item);
  }

  remove(array, item) {
    this[array].splice(item, 1);
  }

  change(array, item, property, value){
    this[array][item][property] = value;
  }

  destroy(property){
    this[property].length = 0;
  }

  get(property) {
      return this[property];
  }

  login(data){
    this.session.user = data.user;
    this.session.email = data.email;
  }
}




//item item-block item-md item-input ng-valid ng-touched input-has-value ng-dirty

//item item-block item-md item-input ng-valid ng-touched input-has-value ng-dirty
