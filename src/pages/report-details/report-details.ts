import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Toast } from '@ionic-native/toast';
import { Globals } from '../../providers/globals';
import { DetailsPage } from '../details/details';



@Component({
  selector: 'page-report-details',
  templateUrl: 'report-details.html',
})
export class ReportDetails {

  public report = {
    id: null,
    title: null,
    note: null,
    approved: false,
    user: null,
    expenses: []
  };

  public image: string = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, public toast: Toast, public globals: Globals) {
    let data = {
      id: this.navParams.get("id"),
      title: this.navParams.get("title"),
      note: this.navParams.get("note"),
      approved: this.navParams.get("approved"),
      user: this.navParams.get("user"),
      expenses: this.navParams.get("expenses")
    };

    this.report = data;
    console.log(this.report)
  }

  details(e) {
    this.navCtrl.push(DetailsPage, e);
  }
}
