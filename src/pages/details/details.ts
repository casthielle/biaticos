import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { Toast } from '@ionic-native/toast';
//import Tesseract from 'tesseract.js';
import { Globals } from '../../providers/globals';

@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {

  adjust: string = "adjust";
  expense: object = {};
  progress: number = 0;
  status: string = "";
  preload: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public camera: Camera, public toast: Toast, public globals: Globals) {
    let data = {
      provider: this.navParams.get("provider"),
      image: this.navParams.get("image"),
      description: this.navParams.get("description"),
      date: this.navParams.get("date"),
      category: this.navParams.get("category"),
      ticket: this.navParams.get("ticket"),
      amount: this.navParams.get("amount"),
      currency: this.navParams.get("currency"),
      ccoo: this.navParams.get("ccoo"),
      note: this.navParams.get("note"),
      reported: this.navParams.get("reported"),
      reportID: this.navParams.get("reportID")
    };
    if(data.image === "assets/file-broken.png"){
      this.adjust = "default";
    }
    else{
      this.adjust = "adjust";
    }
    this.expense = data;
  }

  openCamera() {
    const options = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.CAMERA,
      mediaType: this.camera.MediaType.PICTURE,
      encodingType: this.camera.EncodingType.JPEG,
      allowEdit: true,
      targetWidth: 1920,
    };
    this.camera.getPicture(options).then((image) => {
      this.adjust = "adjust"
      console.log(image);
    }, (err) => {
      console.log(err);
    });
  }

}
