import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, AlertController } from 'ionic-angular';
import { AddPage } from '../add/add';
import { DetailsPage } from '../details/details';
import { ReportPage } from '../report/report';
import { Globals } from '../../providers/globals';
import { AndroidPermissions } from '@ionic-native/android-permissions';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  searchQuery: string = '';

  public expenses: Array<{
    provider: string,
    image: string,
    description: string,
    date: string,
    category: string,
    ticket: string,
    amount: number,
    currency: string,
    ccoo: string,
    note: string,
    reported: boolean,
    reportID: string
  }>;

  public image: string = null;

  constructor(
    public alert: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public actionCtrl: ActionSheetController,
    public globals: Globals,
    private androidPermissions: AndroidPermissions) {
    this.init();
  };


  init(){
    this.expenses = this.globals.get('expenses');
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
      result => console.log('Has permission?',result.hasPermission),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
    );
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
  }

  getItems(ev: any) {

    this.init();
    let val = ev.target.value;

    if (val && val.trim() != '') {
      this.expenses = this.expenses.filter((item) => {
        return (item.description.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  addExpense(){
    this.navCtrl.push(AddPage);
  }

  details(e){
    this.navCtrl.push(DetailsPage, e);
  }

  addReport(){
    let not_reported = 0;

    for(let item of this.expenses){
      if(!item.reported){
        not_reported = not_reported + 1;
      }
    }

    if(not_reported === 0){
      let alert = this.alert.create({
        subTitle: 'No hay gastos para informar!',
        buttons: ['Cerrar']
      });
      alert.present();
    }
    else{
      this.navCtrl.push(ReportPage);
    }
  }
}
