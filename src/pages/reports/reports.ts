import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ItemSliding } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';
import { Globals } from '../../providers/globals';
import { ReportDetails } from '../report-details/report-details';

@Component({
  selector: 'page-reports',
  templateUrl: 'reports.html',
})
export class ReportsPage {

  public expenses: Array<{
    provider: string,
    image: string,
    description: string,
    date: string,
    category: string,
    ticket: string,
    amount: number,
    currency: string,
    ccoo: string,
    note: string,
    reported: boolean,
    reportID: string
  }>;

  public reports: Array<{
    id: string,
    title: string,
    note: string,
    approved: boolean,
    user: string,
    total: number
  }> = [];

  constructor(public alert: AlertController, public toast: Toast, public globals: Globals, public navCtrl: NavController, public navParams: NavParams) {
    this.expenses = this.globals.get('expenses');
    this.reports = this.globals.get('reports');

    for(let r of this.reports){
      let total = 0;
      for(let e of this.expenses){
        if(e.reportID === r.id){
          total = total + e.amount;
        }
      }
      r.total = total;
    }
  }

  details(report){
    let expenses = [];
    for(let expense of this.expenses){
      if(expense.reportID === report.id){
        expenses.push(expense);
      }
    }
    report.expenses = expenses;
    this.navCtrl.push(ReportDetails, report);
  }

  approve(report, status, slidingItem: ItemSliding){
    report.approved = status;
    console.log(this.reports);
    slidingItem.close();

    let title;
    let message;

    if(status){
      title = "Aprobado!";
      message = "El informe ha sido aprobado.";
    }
    else{
      title = "Rechazado!";
      message = "El informe ha sido rechazado.";
    }

    let alert = this.alert.create({
      title: title,
      subTitle: message,
      buttons: ['Cerrar']
    });
    alert.present();

  }

}
