import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Toast } from '@ionic-native/toast';
import { Keyboard } from '@ionic-native/keyboard';
import { Globals } from '../../providers/globals';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {

  public session = { user: "", password: "" };

  constructor(public globals: Globals, private keyboard: Keyboard, public toast: Toast, public navCtrl: NavController, public navParams: NavParams) {
    this.keyboard.disableScroll(false);
  }

  login(){
    if(this.session.user.toLowerCase() === "admin" && this.session.password ===  "admin"){
      this.globals.login({user: this.session.user.toLowerCase(), email: this.session.user.toLowerCase()});
      this.navCtrl.setRoot(HomePage);
    }
    else{
      this.toast.show('Credenciales invalidas.', '4000', 'bottom').subscribe(
        toast => {
          console.log(toast);
        }
      );
    }
  }

}
