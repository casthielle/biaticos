import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';
import { Globals } from '../../providers/globals';


@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
})
export class ReportPage {

  public id: string;

  public expenses: Array<{
    provider: string,
    image: string,
    description: string,
    date: string,
    category: string,
    ticket: string,
    amount: number,
    currency: string,
    ccoo: string,
    note: string,
    reported: boolean,
    reportID: string
  }>;

  public reports: Array<{
    id: string,
    title: string,
    note: string,
    approved: boolean,
    user: string,
    date: string
  }> = [];

  public report = {
    id: null,
    title: null,
    note: null,
    approved: false,
    user: null,
    date: null
  };

  constructor(public alert: AlertController, public toast: Toast, public globals: Globals, public navCtrl: NavController, public navParams: NavParams) {
    this.expenses = this.globals.get('expenses');
    this.reports = this.globals.get('reports');
  }

  save(){

    let date = new Date();
    let dateFormatted = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate();

    this.report.id = (this.reports.length + 1).toString();
    this.report.date = dateFormatted;

    for(let item of this.expenses){
      if(item.reported==true && item.reportID == null){
        item.reportID = this.report.id;
      }
    }

    this.globals.add('reports', this.report);

    let alert = this.alert.create({
      title: 'Listo!',
      subTitle: 'El informe has sido enviado correctamente.',
      buttons: ['Cerrar']
    });
    alert.present();

    this.navCtrl.pop();
  }

}
