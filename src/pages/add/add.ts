import { Component, ViewChild } from '@angular/core';
import { NavController, LoadingController, ActionSheetController, Content } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { Toast } from '@ionic-native/toast';
import Tesseract from 'tesseract.js';
import { Globals } from '../../providers/globals';


@Component({
  selector: 'page-add',
  templateUrl: 'add.html'
})

export class AddPage {

  @ViewChild(Content) content: Content;

  image: string = "assets/file-broken.png?" + new Date().getTime();
  adjust: string = "default";
  progress: number = 0;
  status: string = "";
  preload: string;

  expense = {
    provider: null,
    image: null,
    description: null,
    date: null,
    category: "Transporte",
    ticket: null,
    amount: null,
    currency: "clp",
    ccoo: "Operaciones",
    note: null,
    reported: false,
    reportID: null
  };

  constructor(
    public actionSheet: ActionSheetController,
    public toast: Toast,
    public navCtrl: NavController,
    private camera: Camera,
    private loadingCtrl: LoadingController,
    public globals: Globals
  ){}

  openCamera(source) {
    const options = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: source,
      mediaType: this.camera.MediaType.PICTURE,
      encodingType: this.camera.EncodingType.JPEG,
      allowEdit: true,
      targetWidth: 1920,
    };
    this.camera.getPicture(options).then((image) => {
      this.image = image + "?" + new Date().getTime();
      this.adjust = "adjust"
      console.log(image);
    }, (err) => {
      console.log(err);
    });
  }

  getData(){
    let loading = this.loadingCtrl.create({ content: "Espere..."});
    loading.present();
    Tesseract.recognize(this.image, { lang: 'spa' }).progress((progress) => {
      this.status = progress.status;
      this.progress = Math.ceil(progress.progress * 100);
      //this.preload = "custom-preload";
      console.log(this.progress + "% - " + this.status);
    }).catch(err => {
      console.log(err + ' - ERROR CRITICO.');
      loading.dismiss();
    }).then(result => {
      if(result.lines.length > 0){ this.extract(result.lines); }
      else{ this.toast.show('no se encontraron datos', '5000', 'bottom'); };
      this.status = "Extraccion Finalizada";
      this.progress = 0;
      this.scrollToBottom(500);
      loading.dismiss();
      console.log(this.expense);
    });
  }

  action(){
    let actionSheet = this.actionSheet.create({
      title: 'Seleccione el origen',
      buttons: [
        {
          text: 'Camara',
          handler: () => {
            this.openCamera(this.camera.PictureSourceType.CAMERA);
          }
        },{
          text: 'Galeria',
          handler: () => {
            this.openCamera(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        }
      ]
    });
    actionSheet.present();
  }

  scrollToBottom(duration){
    this.content.scrollToBottom(duration);
  }

  extract(text){
    this.expense.provider = this.polish(text[0].text);
    this.expense.image = this.image;
    this.expense.description = "Gastos Varios";
    this.expense.reported = false;

    for(let line of text){
      if(this.polish(line.words[0].text.toLowerCase()) === "total"){
        this.expense.amount = parseFloat(this.polish(line.words[1].text));
      }
      if(this.polish(line.words[0].text.toLowerCase()) === "fecha"){
        var date = this.polish(line.words[1].text).split('/');

        this.expense.date = date[2] + "-" + date[1] + "-" + date[0];
      }
      if(this.polish(line.words[0].text.toLowerCase()).replace(' ','') === "nro." || this.polish(line.words[0].text.toLowerCase()).replace(' ','') === "nro.t." ){
        this.expense.ticket = this.polish(line.words[line.words.length - 1].text);
      }
    }
  }

  polish(text){
    let polished = text.replace(/[^a-zA-Z 0-9.,/]+/g,'').trim();
    polished = polished.replace(/[áäà]/gi,"a");
    polished = polished.replace(/[éëè]/gi,"e");
    polished = polished.replace(/[íïì]/gi,"i");
    polished = polished.replace(/[óöò]/gi,"o");
    polished = polished.replace(/[úüù]/gi,"u");
    polished = polished.replace(/ñ/gi,"n");
    return polished;
  }

  save(){
    console.log(this.expense);
    this.globals.add('expenses', this.expense);
    this.image = "assets/file-broken.png";
    this.expense = {
      provider: null,
      image: null,
      description: null,
      date: null,
      category: "Transporte",
      ticket: null,
      amount: null,
      currency: "clp",
      ccoo: "Operaciones",
      note: null,
      reported: false,
      reportID: null,
    };
    this.navCtrl.pop();
  }
}
